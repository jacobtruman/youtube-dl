# coding: utf-8
from __future__ import unicode_literals

import re

from .common import InfoExtractor
from ..utils import (
    int_or_none,
    determine_ext,
    parse_age_limit,
)
import random
import string
import sys
import json


class PlutoIE(InfoExtractor):
    def _get_automatic_captions(self, *args, **kwargs):
        pass

    def _mark_watched(self, *args, **kwargs):
        pass

    def _get_subtitles(self, *args, **kwargs):
        pass

    _VALID_URL = r'https?://(?:www\.)?pluto\.tv/on-demand/(?:series/|movies/)'

    _TESTS = [
        {
            'url': 'http://pluto.tv/on-demand/series/3rd-rock-from-the-sun/season/1/episode/brains-and-eggs-1-1',
            'info_dict': {
                'id': 'brains-and-eggs-1-1',
                'ext': 'mp4',
                'title': 'Brains and Eggs',
                'description': 'md5:248bcce3a7f8ddeb71bd6c91f65df598',
            },
            'params': {
                # m3u8 download
                'skip_download': True,
            },
        },
        {
            'url': 'http://pluto.tv/on-demand/movies/beverly-hills-cop-iii-1-1',
            'info_dict': {
                'id': 'beverly-hills-cop-iii-1-1',
                'ext': 'mp4',
                'title': 'Brains and Eggs',
                'description': 'md5:248bcce3a7f8ddeb71bd6c91f65df598',
            },
            'params': {
                # m3u8 download
                'skip_download': True,
            },
        }
    ]

    def _extract_videos(self, video_slug='-1', show_id='-1'):
        slugs_json = self._download_json("http://api.pluto.tv/v3/vod/slugs/{0}".format(show_id), video_slug)
        for season in slugs_json['seasons']:
            for episode in season['episodes']:
                if episode['slug'] == video_slug:
                    episode['show_name'] = slugs_json.get('name') or None
                    return episode

    @staticmethod
    def random_string(string_length=10):
        """Generate a random string of fixed length """
        alpha_numeric = string.ascii_lowercase + string.octdigits
        return ''.join(random.choice(alpha_numeric) for i in range(string_length))

    def _real_extract(self, url):
        url_slugs = url.rsplit('/', 5)
        video_slug = url_slugs[-1]
        show_id = url_slugs[1]

        sid = '-'.join(self.random_string(i) for i in [8, 4, 4, 4, 12])

        if show_id:
            video_data = self._extract_videos(video_slug, show_id)
            assets = video_data.get('stitched', {}).get('urls', [])
        else:
            video_data = self._download_json("http://api.pluto.tv/v3/vod/slugs/{0}".format(video_slug), video_slug)
            assets = [{
                "url": "https://service-stitcher.clusters.pluto.tv/stitch/hls/episode/{0}/master.m3u8?deviceType=&deviceMake=&deviceModel=&sid={1}&deviceId=&deviceVersion=&appVersion=&deviceDNT=0".format(
                    video_data['_id'], sid)}]

        video_id = video_data['_id']
        title = video_data['name']

        formats = []
        for asset in assets:
            asset_url = asset.get('url')
            if not asset_url:
                continue
            if "&sid=&" in asset_url:
                asset_url = asset_url.replace("&sid=&", "&sid=1234&")
            format_id = asset.get('type')
            ext = determine_ext(asset_url)
            if ext == 'm3u8':
                formats.extend(self._extract_m3u8_formats(
                    asset_url, video_id, 'mp4', m3u8_id=format_id or 'hls', fatal=False))
            else:
                f = {
                    'format_id': format_id,
                    'url': asset_url,
                    'ext': ext,
                }
                if re.search(r'(?:/mp4/source/|_source\.mp4)', asset_url):
                    f.update({
                        'format_id': ('%s-' % format_id if format_id else '') + 'SOURCE',
                        'preference': 1,
                    })
                else:
                    mobj = re.search(r'/(\d+)x(\d+)/', asset_url)
                    if mobj:
                        height = int(mobj.group(2))
                        f.update({
                            'format_id': ('%s-' % format_id if format_id else '') + '%dP' % height,
                            'width': int(mobj.group(1)),
                            'height': height,
                        })
                formats.append(f)
        self._sort_formats(formats)

        subtitles = {}
        for cc in video_data.get('closedcaption', {}).get('src', []):
            cc_url = cc.get('value')
            if not cc_url:
                continue
            ext = determine_ext(cc_url)
            if ext == 'xml':
                ext = 'ttml'
            subtitles.setdefault(cc.get('lang'), []).append({
                'url': cc_url,
                'ext': ext,
            })

        thumbnails = []
        for thumbnail in video_data.get('thumbnails', {}).get('thumbnail', []):
            thumbnail_url = thumbnail.get('value')
            if not thumbnail_url:
                continue
            thumbnails.append({
                'url': thumbnail_url,
                'width': int_or_none(thumbnail.get('width')),
                'height': int_or_none(thumbnail.get('height')),
            })

        return {
            'id': video_id,
            'title': title,
            'description': video_data.get('description'),
            'duration': int_or_none(video_data.get('duration', {}), 1000),
            'age_limit': parse_age_limit(video_data.get('rating')),
            'episode_number': int_or_none(video_data.get('number')),
            'series': video_data.get('show_name'),
            'season_number': int_or_none(video_data.get('season', {})),
            'thumbnails': thumbnails,
            'formats': formats,
            'subtitles': subtitles,
        }
